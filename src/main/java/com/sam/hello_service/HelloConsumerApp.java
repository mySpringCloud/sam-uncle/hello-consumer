package com.sam.hello_service;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/***
 * 
 * @SpringCloudApplication 一个注解相当于以下3个注解
 * @EnableDiscoveryClient 让服务使用eureka服务器 实现服务注册和发现
 * @SpringBootApplication
 * @EnableCircuitBreaker
 */
@SpringCloudApplication
public class HelloConsumerApp {
	// @Bean 应用在方法上，用来将方法返回值设为为bean
	@Bean
	@LoadBalanced // @LoadBalanced实现负载均衡
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(HelloConsumerApp.class, args);
	}
}
